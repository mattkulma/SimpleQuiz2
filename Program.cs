﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleQuiz
{
    class Program
    {
        static void Main(string[] args)
        {
            int rightAnswers = 0;
            Console.WriteLine("Welcome to the simple quiz\n there are three questions.");
            Console.WriteLine("Press Enter to continue.");
            Console.ReadLine();



            //question 1
            Console.WriteLine("First Question");
            Console.WriteLine("What is the first derivative of X^3+X-5?");
            Console.WriteLine("(1) 3X-4\n"+"(2) x^4+2X\n"+"(3) 3X^2+1\n"+"(4) 6X+9");
            string ansOne = Console.ReadLine();
            if (ansOne == "3")
            {
                rightAnswers++;
            }


            //question 2
            Console.WriteLine("Second Question");
            Console.WriteLine("what are the Values of X: X^2 + 6X + 9?");
            Console.WriteLine("(1) X=3\n"+"(2) X=4\n"+"(3) X=-3\n"+"(4) X=6");
            string ansTwo = Console.ReadLine();
            if (ansTwo == "1")
            {
                rightAnswers++;
            }


            //question 3
            Console.WriteLine("Third Question");
            Console.WriteLine("What is the Absolute Value of 9.8637");
            Console.WriteLine("(1) 9\n"+"(2) 98637\n"+"(3) 98.63\n"+"(4) 9.86");
            string ansThree = Console.ReadLine();
            if (ansThree == "1")
            {
                rightAnswers++;
            }

           


            Console.WriteLine("You got {0} right!", rightAnswers);
            Console.ReadLine();

        }
    }
}
